import unittest
import qdumper

class ThisIsAClass:
    a = 1
    def method(self):
        pass

class TestDumper(unittest.TestCase):
    a = 1
    b = 2
    c = 3
    def test_dumper(self):
        self.d = 4
        t = qdumper.dump( self )
        print( t )
        o = ThisIsAClass()
        o.a = 1
        t = qdumper.dump( o )
        print( t )

if __name__ == '__main__':
    unittest.main()
