from setuptools import setup, find_packages

setup(
    name = "quoonel-dumper",
    version = "0.1",
    packages = find_packages()
)
