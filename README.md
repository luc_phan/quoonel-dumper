# quoonel-dumper #

### Description ###

Dump data structures

### Usage ###

```
$ vim test.py
```

```
#!python
#!/usr/bin/env python3

import qdumper

class Demo:
    pass

d = Demo()
d.a = 1
d.b = [ 2, 3, 4 ]
d.c = { 5:6, 7:8 }
print( qdumper.dump(d) )
```

```
$ python test.py
object __main__.Demo:
  a = 1
  b =
    [
      2,
      3,
      4,
    ]
  c =
    {
      5: 6,
      7: 8,
    }
```

### Installation ###

```
$ cat /etc/system-release
CentOS Linux release 7.0.1406 (Core)
```

Python installation :

```
$ sudo yum install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
$ sudo yum install python34u
```

Package installation :

```
$ mkdir new
$ cd new
$ echo "Download quoonel-dumper-0.1.tar.gz"
$ sudo pip3 install quoonel-dumper-0.1.tar.gz
```

### Development ###

```
$ cat /etc/system-release
CentOS Linux release 7.0.1406 (Core)
```

Python installation :

```
$ sudo yum install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
$ sudo yum install python34u
```

Python environment :

```
$ sudo pip3 install virtualenv
$ sudo pip3 install virtualenvwrapper
$ echo export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 >> ~/.bashrc
$ echo source /usr/bin/virtualenvwrapper.sh >> ~/.bashrc
$ source ~/.bashrc
$ mkvirtualenv local -p /usr/bin/python3
```

Cloning :

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/quoonel-dumper.git'
```

Testing :

```
$ cd ~/work/quoonel-dumper
$ python -m unittest
```