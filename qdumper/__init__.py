def dump( o ):
    lines = _dump( o )
    return '\n'.join(lines)

def _dump( o ):
    lines = []
    if hasattr( o, '__module__' ):
        lines.append(
            'object {}.{}:'.format( o.__module__, o.__class__.__name__ )
        )
        if hasattr( o, '__dict__' ):
            for k,v in o.__dict__.items():
                v = _dump( v )
                if len(v) == 1:
                    lines.append( '  {} = {}'.format( k, v[0] ) )
                else:
                    v = map( lambda line: '    ' + line, v )
                    lines.append( '  {} ='.format( k ) )
                    lines.extend( v )
    elif type(o).__name__ == 'list':
        if not o:
            lines.append( '[]' )
        else:
            lines.append( '[' )
            for e in o:
                lines.append( '  ' + str(e) + ',' )
            lines.append( ']' )
    elif type(o).__name__ == 'dict':
        if not o:
            lines.append( '{}' )
        else:
            lines.append( '{' )
            for k,v in o.items():
                lines.append( '  {}: {},'.format( k, v ) )
            lines.append( '}' )
    else:
        lines.append( repr(o) )
    return lines
